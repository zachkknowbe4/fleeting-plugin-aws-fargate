# Fleeting Plugin AWS Fargate

This is a [fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting) plugin for AWS Fargate.

[![Pipeline Status](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws/badges/main/pipeline.svg)](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws)](https://goreportcard.com/report/gitlab.com/gitlab-org/fleeting/fleeting-plugin-aws)

## Building the plugin

To generate the binary, ensure `$GOPATH/bin` is on your PATH, then use `go install`:

```shell
cd cmd/fleeting-plugin-aws-fargate/
go install 
```

If you are managing go versions with asdf, run this after generating the binary:

```shell
asdf reshim
```

## Plugin Configuration

The following parameters are supported:

| Parameter             | Type   | Description |
|-----------------------|--------|-------------|
| `name`                | string | Name of the Auto Scaling Group |
| `profile` | string | Optional. AWS profile-name ([Named profiles for the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)). |
| `config_file`    | string | Optional. Path to the AWS config file ([AWS Configuration and credential file settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)). |
| `credentials_file`    | string | Optional. Path to the AWS credential file ([AWS Configuration and credential file settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)). |

The credentials don't need to be set if the plugin is running on an instance inside AWS
with the IAM permission assigned. See [Recommended IAM Policy](#recommended-iam-policy)

### Default connector config

| Parameter                | Default  |
|--------------------------|----------|
| `os`                     | `linux`  |
| `protocol`               | `ssh` or `winrm` if Windows OS is detected |
| `username`               | `ec2-user` or `Administrator` if Windows OS is detected |
| `use_static_credentials` | `false`  |
| `key_path`               | None. This is the path for the private key file used to connect to the runner **manager** machine. Required for Windows OS. |

For Windows instances, if `use_static_credentials` is false, the password field is populated with a password that AWS provisions.

For other instances, if `use_static_credentials` is false, credentials will be set using [SendSSHPublicKey](https://docs.aws.amazon.com/ec2-instance-connect/latest/APIReference/API_SendSSHPublicKey.html), either using the specified key or dynamically creating one.

## Autoscaling Group Setup

- Group size desired and minimal capacity should be zero.
- Maximum capacity should be equal or more than the configured fleeting Max Size option.
- Scaling policy should be set to `None`.
- Process `AZRebalance` should be suspended.
- Instance scale-in protection should be enabled.

## Setting an IAM policy

### Our recommendations

- Grant least privilege
- Create an IAM group with a policy like the example below and assign each AWS user to the group
- Use policy conditions for extra security. This will depend on your setup.
- Do not share AWS access keys to separate deployments.

Create an `AWS Credential Type` of `Access key - Programmatic access`, enabling the plugin to
access your ASG via the AWS SDK.

## Examples

### GitLab Runner

GitLab Runner has examples on using this plugin for the [Instance executor](https://docs.gitlab.com/runner/executors/instance.html#examples) and [Docker Autoscaler executor](https://docs.gitlab.com/runner/executors/docker_autoscaler.html#examples).
