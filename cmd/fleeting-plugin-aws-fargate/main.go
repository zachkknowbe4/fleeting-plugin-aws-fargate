package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
	aws "gitlab.internal.knowbe4.com/devex/gitlab/fleeting/fleeting-plugin-aws-fargate"
)

var (
	showVersion = flag.Bool("version", false, "Show version information and exit")
)

func main() {
	flag.Parse()
	if *showVersion {
		fmt.Println(aws.Version.Full())
		os.Exit(0)
	}

	plugin.Serve(&aws.InstanceGroup{})
}
