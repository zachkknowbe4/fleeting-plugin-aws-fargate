package aws

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ecs"
	"github.com/aws/aws-sdk-go-v2/service/ecs/types"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	"gitlab.internal.knowbe4.com/devex/gitlab/fleeting/fleeting-plugin-aws-fargate/internal/awsclient"
	"path"
	"time"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

var newClient = awsclient.New

type InstanceGroup struct {
	Profile         string `json:"profile"`
	ConfigFile      string `json:"config_file"`
	CredentialsFile string `json:"credentials_file"`
	Name            string `json:"name"`
	ClusterName     string `json:"cluster_name"`

	log    hclog.Logger
	client awsclient.Client
	size   int

	settings provider.Settings
}

func (g *InstanceGroup) Init(ctx context.Context, log hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	var opts []func(*config.LoadOptions) error

	if g.Profile != "" {
		opts = append(opts, config.WithSharedConfigProfile(g.Profile))
	}
	if g.ConfigFile != "" {
		opts = append(opts, config.WithSharedConfigFiles([]string{g.ConfigFile}))
	}
	if g.CredentialsFile != "" {
		opts = append(opts, config.WithSharedCredentialsFiles([]string{g.CredentialsFile}))
	}
	// Ensure that the SDK retries requests with appropriate backoff logic.
	opts = append(opts, config.WithRetryMode(aws.RetryModeAdaptive))

	cfg, err := config.LoadDefaultConfig(ctx, opts...)
	if err != nil {
		return provider.ProviderInfo{}, fmt.Errorf("creating aws config: %w", err)
	}

	g.client = newClient(cfg)
	g.log = log.With("region", cfg.Region, "name", g.Name)
	g.settings = settings

	if _, err := g.getInstances(ctx, true); err != nil {
		return provider.ProviderInfo{}, err
	}

	return provider.ProviderInfo{
		ID:        path.Join("aws", cfg.Region, g.Name),
		MaxSize:   1000,
		Version:   Version.String(),
		BuildInfo: Version.BuildInfo(),
	}, nil
}

func (g *InstanceGroup) updateTaskProtection(ctx context.Context, instances []types.Task) {
	var chunks [][]string

	for i := 0; i < len(instances); i += 10 {
		end := i + 10
		if end > len(instances) {
			end = len(instances)
		}
		var chunkNames []string
		for _, p := range instances[i:end] {
			lastStatus := *p.LastStatus
			if lastStatus == "RUNNING" {
				taskArn := *p.TaskArn
				chunkNames = append(chunkNames, taskArn)
			}
		}
		if len(chunkNames) > 0 {
			chunks = append(chunks, chunkNames)
		}
	}

	expiration := int32((8 * time.Hour).Minutes())

	for _, chunk := range chunks {
		_, err := g.client.UpdateTaskProtection(ctx, &ecs.UpdateTaskProtectionInput{
			Cluster:           aws.String(g.ClusterName),
			Tasks:             chunk,
			ProtectionEnabled: true,
			ExpiresInMinutes:  aws.Int32(expiration),
		})
		if err != nil {
			g.log.Error("failed to update task protection ", fmt.Errorf("error: %w", err))
		}
	}
}

func (g *InstanceGroup) Update(ctx context.Context, update func(id string, state provider.State)) error {
	instances, err := g.getInstances(ctx, false)
	if err != nil {
		return err
	}

	g.updateTaskProtection(ctx, instances)

	for _, instance := range instances {
		state := provider.StateCreating
		status := *instance.LastStatus
		containerStatus := *instance.Containers[0].LastStatus
		desiredStatus := *instance.DesiredStatus

		switch status {
		case "STOPPED", "FAILED", "DEPROVISIONING":
			state = provider.StateDeleting

		case "RUNNING":
			if containerStatus == "RUNNING" {
				state = provider.StateRunning
			}
		}
		if desiredStatus == "STOPPED" {
			state = provider.StateDeleting
		}
		update(*instance.TaskArn, state)
	}

	return nil
}

func (g *InstanceGroup) Increase(ctx context.Context, delta int) (int, error) {
	_, err := g.client.SetServiceDesiredCount(ctx, &ecs.UpdateServiceInput{
		Service:      aws.String(g.Name),
		DesiredCount: aws.Int32(int32(g.size + delta)),
		Cluster:      aws.String(g.ClusterName),
	})
	if err != nil {
		return 0, fmt.Errorf("increase instances: %w", err)
	}

	g.size += delta

	return delta, nil
}

func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) ([]string, error) {
	if len(instances) == 0 {
		return nil, nil
	}

	var succeeded []string
	for _, instance := range instances {
		_, err := g.client.StopTask(ctx, &ecs.StopTaskInput{
			Task:    aws.String(instance),
			Cluster: aws.String(g.ClusterName),
			Reason:  aws.String("Gitlab autoscaler termination"),
		})
		// We don't want to return the error here because the service size will not decrease
		g.size--
		_, err2 := g.client.SetServiceDesiredCount(ctx, &ecs.UpdateServiceInput{
			Service:      aws.String(g.Name),
			DesiredCount: aws.Int32(int32(g.size)),
			Cluster:      aws.String(g.ClusterName),
		})
		if err != nil {
			return succeeded, err
		}
		succeeded = append(succeeded, instance)
		if err2 != nil {
			return succeeded, err2
		}
	}

	return succeeded, nil
}

func (g *InstanceGroup) getInstances(ctx context.Context, initial bool) ([]types.Task, error) {

	desc, err := g.client.DescribeServices(ctx, &ecs.DescribeServicesInput{
		Services: []string{g.Name},
		Cluster:  aws.String(g.ClusterName),
	})

	if err != nil {
		return nil, fmt.Errorf("describing services: %w", err)
	}
	if len(desc.Services) != 1 {
		return nil, fmt.Errorf("unexpected number of services returned for cluster %s: %v", g.ClusterName, len(desc.Services))
	}

	// detect out-of-sync capacity changes
	service := desc.Services[0]
	capacity := service.DesiredCount
	var size int
	if capacity != 0 {
		size = int(capacity)
	}

	taskArns, err := g.client.ListTasks(ctx, &ecs.ListTasksInput{
		Cluster:     aws.String(g.ClusterName),
		ServiceName: aws.String(g.Name),
	})

	if taskArns == nil {
		return nil, fmt.Errorf("unexpected list tasks response for %s: %v", g.ClusterName, len(desc.Services))
	}

	if err != nil {
		return nil, fmt.Errorf("listing service tasks: %w", err)
	}

	var descTasks *ecs.DescribeTasksOutput
	var tasks []types.Task
	if len(taskArns.TaskArns) > 0 {
		descTasks, err = g.client.DescribeTasks(ctx, &ecs.DescribeTasksInput{
			Cluster: aws.String(g.ClusterName),
			Tasks:   taskArns.TaskArns,
		})

		if err != nil {
			return nil, fmt.Errorf("describing service tasks: %w", err)
		}
		
		tasks = descTasks.Tasks
	}


	if !initial && size != g.size {
		g.log.Error("out-of-sync capacity", "expected", g.size, "actual", size)
	}
	g.size = size

	return tasks, nil
}

func (g *InstanceGroup) ConnectInfo(ctx context.Context, id string) (provider.ConnectInfo, error) {
	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}

	output, err := g.client.DescribeTasks(ctx, &ecs.DescribeTasksInput{
		Tasks:   []string{id},
		Cluster: aws.String(g.ClusterName),
	})
	if err != nil {
		return info, fmt.Errorf("fetching instance: %w", err)
	}

	if len(output.Tasks) == 0 {
		return info, fmt.Errorf("fetching tasks: not found")
	}

	instance := output.Tasks[0]
	taskDefintionOutput, err := g.client.DescribeTaskDefinition(ctx, &ecs.DescribeTaskDefinitionInput{
		TaskDefinition: instance.TaskDefinitionArn,
	})

	if err != nil {
		return info, fmt.Errorf("fetching task defintion: %w", err)
	}

	taskDefintion := taskDefintionOutput.TaskDefinition

	if info.OS == "" {
		switch {
		default:
			info.OS = "linux"
		}
	}

	info.InternalAddr = aws.ToString(instance.Containers[0].NetworkInterfaces[0].PrivateIpv4Address)
	info.ExternalAddr = ""

	if info.Arch == "" {
		switch taskDefintion.RuntimePlatform.CpuArchitecture {
		case types.CPUArchitectureX8664:
			info.Arch = "amd64"
		case types.CPUArchitectureArm64:
			info.Arch = "arm64"
		}
	}

	err = g.ssh(&info)

	return info, err
}

func (g *InstanceGroup) Shutdown(ctx context.Context) error {
	return nil
}
