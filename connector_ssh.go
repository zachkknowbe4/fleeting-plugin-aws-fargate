package aws

import (
	"crypto"
	"fmt"
	"time"

	"golang.org/x/crypto/ssh"

	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type PrivPub interface {
	crypto.PrivateKey
	Public() crypto.PublicKey
}

func (g *InstanceGroup) ssh(info *provider.ConnectInfo) error {
	if info.Key == nil {
		return fmt.Errorf("key must be provided for fargate")
	}

	priv, err := ssh.ParseRawPrivateKey(info.Key)
	if err != nil {
		return fmt.Errorf("reading private key: %w", err)
	}
	var ok bool
	_, ok = priv.(PrivPub)
	if !ok {
		return fmt.Errorf("key doesn't export PublicKey()")
	}

	expires := time.Now().Add(60 * time.Second)
	info.Expires = &expires

	return nil
}
