package fake

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/service/ecs"
	"github.com/aws/aws-sdk-go-v2/service/ecs/types"
	"sync"
	"sync/atomic"

	"github.com/aws/aws-sdk-go-v2/aws"
)

type Instance struct {
	InstanceId string
	State      string
}

type Client struct {
	cfg aws.Config

	Name         string
	Instances    []Instance
	DesiredCount int

	count atomic.Uint64
}

var once sync.Once
var winrmKey *rsa.PrivateKey

func Key() *rsa.PrivateKey {
	once.Do(func() {
		var err error
		winrmKey, err = rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			panic(err)
		}
	})

	return winrmKey
}

func New(cfg aws.Config) *Client {
	return &Client{cfg: cfg}
}

func (c *Client) DescribeServices(ctx context.Context, params *ecs.DescribeServicesInput, optFns ...func(*ecs.Options)) (*ecs.DescribeServicesOutput, error) {
	var services []types.Service
	//var instances []types.Task

	//for idx, instance := range c.Instances {
	//	if instance.State == "STOPPED" {
	//		c.Instances = append(c.Instances[:idx], c.Instances[idx+1:]...)
	//		c.DesiredCount--
	//	}
	//}
	//
	//for _, instance := range c.Instances {
	//	instances = append(instances, types.Task{
	//		TaskArn:    &instance.InstanceId,
	//		LastStatus: aws.String(instance.State),
	//	})
	//}
	service := types.Service{
		DesiredCount: int32(c.DesiredCount),
		ServiceName:  aws.String("foobar"),
		ServiceArn:   aws.String("arn:foobar"),
	}
	services = append(services, service)

	return &ecs.DescribeServicesOutput{Services: services}, nil

}

func (c *Client) SetServiceDesiredCount(ctx context.Context, params *ecs.UpdateServiceInput, optFns ...func(*ecs.Options)) (*ecs.UpdateServiceOutput, error) {
	c.DesiredCount = int(aws.ToInt32(params.DesiredCount))

	for len(c.Instances) < c.DesiredCount {
		c.Instances = append(c.Instances, Instance{
			InstanceId: fmt.Sprintf("instance__%d", c.count.Add(1)),
			State:      "RUNNING",
		})
	}

	return &ecs.UpdateServiceOutput{}, nil
}

func (c *Client) StopTask(ctx context.Context, params *ecs.StopTaskInput, optFns ...func(*ecs.Options)) (*ecs.StopTaskOutput, error) {
	for idx, instance := range c.Instances {
		if instance.InstanceId == aws.ToString(params.Task) {
			c.Instances[idx].State = "STOPPED"
		}
	}

	return &ecs.StopTaskOutput{}, nil
}

func (c *Client) DescribeTasks(ctx context.Context, params *ecs.DescribeTasksInput, optFns ...func(*ecs.Options)) (*ecs.DescribeTasksOutput, error) {
	var tasks []types.Task
	for _, instance := range c.Instances {
		tasks = append(tasks, types.Task{
			TaskArn:       &instance.InstanceId,
			LastStatus:    aws.String("RUNNING"),
			DesiredStatus: aws.String("RUNNING"),
			Containers: []types.Container{
				{
					Name:       aws.String("foobar"),
					LastStatus: aws.String("RUNNING"),
					NetworkInterfaces: []types.NetworkInterface{
						{
							PrivateIpv4Address: aws.String("127.0.0.1"),
						},
					},
				},
			},
		})
	}

	return &ecs.DescribeTasksOutput{Tasks: tasks}, nil
}

func (c *Client) UpdateTaskProtection(ctx context.Context, params *ecs.UpdateTaskProtectionInput, optFns ...func(*ecs.Options)) (*ecs.UpdateTaskProtectionOutput, error) {
	var tasks []types.ProtectedTask
	for _, instance := range c.Instances {
		tasks = append(tasks, types.ProtectedTask{
			TaskArn: &instance.InstanceId,
		})
	}

	return &ecs.UpdateTaskProtectionOutput{
		ProtectedTasks: tasks,
	}, nil
}

func (c *Client) ListTasks(ctx context.Context, params *ecs.ListTasksInput, optFns ...func(*ecs.Options)) (*ecs.ListTasksOutput, error) {
	var tasks []string
	for _, instance := range c.Instances {
		tasks = append(tasks, instance.InstanceId)
	}

	return &ecs.ListTasksOutput{
		TaskArns: tasks,
	}, nil
}

func (c *Client) DescribeTaskDefinition(ctx context.Context, params *ecs.DescribeTaskDefinitionInput, optFns ...func(*ecs.Options)) (*ecs.DescribeTaskDefinitionOutput, error) {
	return &ecs.DescribeTaskDefinitionOutput{
		TaskDefinition: &types.TaskDefinition{
			TaskDefinitionArn: params.TaskDefinition,
			RuntimePlatform: &types.RuntimePlatform{
				CpuArchitecture:       types.CPUArchitectureArm64,
				OperatingSystemFamily: types.OSFamilyLinux,
			},
		},
	}, nil
}
