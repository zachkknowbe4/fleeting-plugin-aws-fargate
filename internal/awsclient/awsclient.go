package awsclient

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/autoscaling"
	"github.com/aws/aws-sdk-go-v2/service/ecs"
)

type Client interface {
	DescribeServices(context.Context, *ecs.DescribeServicesInput, ...func(*ecs.Options)) (*ecs.DescribeServicesOutput, error)
	SetServiceDesiredCount(context.Context, *ecs.UpdateServiceInput, ...func(*ecs.Options)) (*ecs.UpdateServiceOutput, error)
	DescribeTasks(context.Context, *ecs.DescribeTasksInput, ...func(*ecs.Options)) (*ecs.DescribeTasksOutput, error)
	UpdateTaskProtection(context.Context, *ecs.UpdateTaskProtectionInput, ...func(*ecs.Options)) (*ecs.UpdateTaskProtectionOutput, error)
	ListTasks(context.Context, *ecs.ListTasksInput, ...func(*ecs.Options)) (*ecs.ListTasksOutput, error)
	DescribeTaskDefinition(context.Context, *ecs.DescribeTaskDefinitionInput, ...func(*ecs.Options)) (*ecs.DescribeTaskDefinitionOutput, error)
	StopTask(context.Context, *ecs.StopTaskInput, ...func(*ecs.Options)) (*ecs.StopTaskOutput, error)
}

var _ Client = (*client)(nil)

type client struct {
	autoscaler *autoscaling.Client
	ecs        *ecs.Client
}

func New(cfg aws.Config) Client {
	return &client{
		autoscaler: autoscaling.NewFromConfig(cfg),
		ecs:        ecs.NewFromConfig(cfg),
	}
}

func (c *client) SetServiceDesiredCount(ctx context.Context, params *ecs.UpdateServiceInput, optFns ...func(*ecs.Options)) (*ecs.UpdateServiceOutput, error) {
	return c.ecs.UpdateService(ctx, params, optFns...)
}

func (c *client) DescribeTasks(ctx context.Context, params *ecs.DescribeTasksInput, optFns ...func(*ecs.Options)) (*ecs.DescribeTasksOutput, error) {
	return c.ecs.DescribeTasks(ctx, params, optFns...)
}

func (c *client) UpdateTaskProtection(ctx context.Context, params *ecs.UpdateTaskProtectionInput, optFns ...func(*ecs.Options)) (*ecs.UpdateTaskProtectionOutput, error) {
	return c.ecs.UpdateTaskProtection(ctx, params, optFns...)
}

func (c *client) DescribeServices(ctx context.Context, params *ecs.DescribeServicesInput, optFns ...func(*ecs.Options)) (*ecs.DescribeServicesOutput, error) {
	return c.ecs.DescribeServices(ctx, params, optFns...)
}

func (c *client) ListTasks(ctx context.Context, params *ecs.ListTasksInput, optFns ...func(*ecs.Options)) (*ecs.ListTasksOutput, error) {
	return c.ecs.ListTasks(ctx, params, optFns...)
}

func (c *client) DescribeTaskDefinition(ctx context.Context, params *ecs.DescribeTaskDefinitionInput, optFns ...func(*ecs.Options)) (*ecs.DescribeTaskDefinitionOutput, error) {
	return c.ecs.DescribeTaskDefinition(ctx, params, optFns...)
}

func (c *client) StopTask(ctx context.Context, params *ecs.StopTaskInput, optFns ...func(*ecs.Options)) (*ecs.StopTaskOutput, error) {
	return c.ecs.StopTask(ctx, params, optFns...)
}
